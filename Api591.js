const Nightmare = require('nightmare')

class Api591 {
    constructor(show) {
        show = show || false
        this.nightmare = Nightmare({
            show: show,
        })
    }
    async queryRent(args) {
        console.log(args)
        return new Promise((resolve, reject) => {
        	let nightmare = this.nightmare

            nightmare
            .goto('https://rent.591.com.tw/?kind=0&region=1')
            .wait('.area-box-close')
            .click('.area-box-close')
            .click('.search-more-ul li:nth-child(2) label')
            .type('.rentPrice-min', args.minPrice)
            .type('.rentPrice-max', args.maxPrice)
            .click('.rentPrice-btn')
            .wait(100)
            .click('.orderSelectList li:nth-child(8)')
            .wait(100)

			if (args.keyword) {
				this._enterKeyword(args.keyword)
				.wait(100)
			}

			if (args.house) {
				this._selectHouseType(args.house)
				.wait(100)
			}

			if (args.rentShape) {
				this._selectRentShape(args.rentShape)
				.wait(100)
			}

			if (args.region) {
				this._selectRegion(args.region)
				.wait(100)
			}

			nightmare
			.refresh()
			.wait(2000)

			let remainPage = args.page || 1
			let result = []
			collectItems()

            function collectItems(data) {
				nightmare
				.evaluate(parseItems)
				.then((data) => {
					result = result.concat(data)
					remainPage--
					if (remainPage <= 0) {
						resolve(result)
					} else {
						nightmare
						.click('.pageNext')
						.wait(2000)
						collectItems(data)
					}
				})
				.catch((error) => {reject(error)})
			}

            function parseItems() {
                let result = []
                $('.listInfo').each(function(i){
                    let item = {
                        name: $(this).find('h3 > a').html().trim(),
                        url: $(this).find('h3 > a').prop('href'),
                        price: $(this).find('.price i').html(),
                        desc: $(this).find('.infoContent > .lightBox').first().text().replace(/\s+/g, ' '),
                        location: $(this).find('.infoContent > .lightBox').last().text().replace(/\s+/g, ' '),
                        status: $($(this).find('.infoContent').children()[3]).text().replace(/\s+/g, ' '),
                        img: $(this).find('img').data().original
                    }

                    if (item.desc.indexOf('4坪') !== -1 ||
                        item.desc.indexOf('5坪') !== -1 ||
                        item.desc.indexOf('6坪') !== -1
                    ) {
                        return 0
                    }

                    if (item.desc.indexOf('4/4') !== -1 || 
                        item.desc.indexOf('4/5') !== -1 ||
                        item.desc.indexOf('5/5') !== -1 
                    ) {
                        return 0
                    }

                    if (item.location.indexOf('淡水') !== -1 ||
                    	item.location.indexOf('蘆洲') !== -1 ||
                    	item.location.indexOf('汐止') !== -1 ||
                    	item.location.indexOf('八里') !== -1 ||
                    	item.location.indexOf('三峽') !== -1 ||
                    	item.location.indexOf('鶯歌') !== -1 ||
                    	item.location.indexOf('土城') !== -1 ||
                    	item.location.indexOf('樹林') !== -1 ||
                    	item.location.indexOf('北投') !== -1 ||
                    	item.location.indexOf('瑞芳') !== -1 ||
                    	item.location.indexOf('三芝') !== -1 ||
                    	item.location.indexOf('士林') !== -1 ||
                    	item.location.indexOf('三重') !== -1 ||
                    	item.location.indexOf('新莊') !== -1 ||
                    	item.location.indexOf('林口') !== -1
                    ) {
                    	return 0
                    }

                    if (item.desc.indexOf('獨立套房') !== -1 || item.desc.indexOf('整層住家') !== -1) {
                        result.push(item)
                    }
                })

                return result
            }
        })
    }

    _selectRegion(region) {
        let nightmare = this.nightmare
       
        if (region === "大安區") {
            nightmare
            .click('.search-location-span[data-index="1"]')
            .click('.city_dl:nth-child(1) .city-li:nth-child(1)')
            .wait('#checktips5')
            .wait(50)
            .click('#checktips5')
        } else if (region === "中山區") {
            nightmare
            .click('.search-location-span[data-index="1"]')
            .click('.city_dl:nth-child(1) .city-li:nth-child(1)')
            .wait('#checktips3')
            .wait(50)
            .click('#checktips3')
        } else if (region === "板橋區") {
            nightmare
            .click('.search-location-span[data-index="1"]')
            .click('.city_dl:nth-child(1) .city-li:nth-child(2)')
            .wait('#checktips26')
            .wait(50)
            .click('#checktips26')
        } else if (region === "台北市") {
            nightmare
            .click('.search-location-span[data-index="1"]')
            .click('.city_dl:nth-child(1) .city-li:nth-child(1)')
        } else if (region === "新北市") {
            nightmare
            .click('.search-location-span[data-index="1"]') 
            .click('.city_dl:nth-child(1) .city-li:nth-child(2)')
        } else {
            throw new Error("Unknown region!!")
        }

        return nightmare
    }

    _enterKeyword(keyword) {
        let nightmare = this.nightmare
        nightmare
        .type('#keywords', keyword)
        .click('span.searchBtn')

        return nightmare
    }

    _selectHouseType(houseType) {
        let nightmare = this.nightmare

    	if (houseType === 'family') {
    		nightmare
    		.click('.search-rentType-span[data-index="1"]')
    	}

    	return nightmare
    }

    _selectRentShape(rentShape) {
        let nightmare = this.nightmare

        if (rentShape === 'elevator') {
        	nightmare.click('#rentShape li label[data-index="2"]')
        }

        return nightmare
    }

    close() {
        this.nightmare.end()
    }
}

export { Api591 as default }
