const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function execOnce(retryTimes) {
	retryTimes = retryTimes || 0
    console.log(new Date().toLocaleString())
    const { stdout, stderr } = await exec('babel-node execute.js');
    console.log('stdout:', stdout);
    console.log('stderr:', stderr);
    console.log('==========')
    if (stderr.length > 0) {
    	console.log(`Consider retrying... $retryTimes`)
    	if (retryTimes <= 3) {
			execOnce(retryTimes + 1)
    	}
    }
}

async function main() {
    execOnce()
    setInterval(execOnce, 60 * 60 * 1000)
}

main()
