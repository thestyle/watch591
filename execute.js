import Api591 from './Api591'
import History from './History'
import NotifierEmail from './NotifierEmail'
import fs from 'fs';

const filename = process.argv[2] || 'config.json'
const config = JSON.parse(fs.readFileSync(filename))
const options = config.options
const api591 = new Api591(config.showUi)
const notifier = new NotifierEmail()
const historys = {}
for (let option of options) {
    historys[option.id] = new History(option.id) }

async function main() {
    let text = new Date().toLocaleString()
    let shouldNotify = false
    for (let option of options) {
        console.log(`query region ${option.region}...`)

        // query
        let items = await api591.queryRent(option)

        // filter out duplicate
        let newItems = []
        for (let item of items) {
            let isNew = true
            for (let history in historys) {
                if (historys[history].find(item.url) !== null) {
                    isNew = false
                    break
                }
            }
            if (isNew) {
                shouldNotify = true
                historys[option.id].save(item.url, item)
                newItems.push(item)
            }
        }

        // generate text
        console.log(`new item number: ${newItems.length}`)
        text += `<h1>${option.name} (${newItems.length})</h1>`
        for (let item of newItems) {
            text += itemToText(item)
        }
        text += '\n'

        if (option.id !== options.slice(-1).id) {
            await sleep(500)
        }
    }

    // sned notification
    if (shouldNotify && config.shouldNotify) {
        await notifier.notify(text)
    }
    api591.close()


	// helpers
	function itemToText(item) {
		return `<h3>${item.name} <span style="color: red;">${item.price}</span></h3>
		<p>${item.desc} <br /> ${item.location} <br /> <span style="color: gray;">${item.status}</span></p>
		<p><img src="${item.img}" /></p>
		<p><a href="${item.url}">${item.url}</a></p>
		<hr />`;
	}

	async function sleep(ms) {
		return new Promise((resolve, reject)=>{
			setTimeout(resolve, ms);
		});
	}
}

async function mainWrapper() {
	let xvfb = null
	const shouldUseXvfb = require('os').type() === 'Linux'
	if (shouldUseXvfb) {
		let XVFB = require('xvfb')
		xvfb = new XVFB()
		xvfb.startSync()
	}
	await main()
	if (xvfb) {
		xvfb.stopSync()
	}
}

mainWrapper().then(() => {
	// do nothing
}).catch(async (error) => {
	console.error(error)
	await notifier.notify(error)
}).finally(() => {
	console.log('Process exit!')
	process.exit()
})
