# Introduction
It can crawl 591 in order to monitor the rent information that you want.

# Usage
## Run once
```
$ babel-node execute.js [config.json]
```

## Keep monitoring
```
$ node index.js
```

# Configuration
## config.json
```
{
	"showUi": (boolean), // if show the website while executing
	"shouldNotify": (boolean), // if notify the result by email
	"options": [OptionType], 
}
```

### OptionType : object
```
{
	"id": (string), 
	"name": (string), // displayed text
	"region" (RegionType),
	"keyword": (string), // the keyword used by searching
	"minPrice": (int),
	"maxPrice": (int),
	"house": (HouseType),
	"page": (int), // the first n-th pages for collecting
	// optional
	"rentShape": (RentShapeType),
}
```

### RegionType : string
```
["台北市", "新北市", "板橋區", "大安區", "中山區"]
```

### HouseType : string
```
["family"]
```

### RentShapeType: string
```
["elevator"]
```

## email.json
Please create a configuration file named `email.json` and set it properly based on `email.example.json`.
