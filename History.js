var LocalStorage = require('node-localstorage').LocalStorage;
  
class History {
    constructor(file) {
        this.localStorage = new LocalStorage(`storage-${file}`);
    }

    find(key) {
        return this.localStorage.getItem(key)
    }

    save(key, value) {
        this.localStorage.setItem(key, value)
    }
}

export { History as default }